import unittest

class TestFinn(unittest.TestCase):
    def test_get_apt_return_valid_data(self):
        from sfe.sfe import get_apt
        apt_url = 'https://www.finn.no/realestate/homes/ad.html?finnkode=113104124'
        apt_id = 113104124
        # TODO: currently it goes out online, should be mocked to use local file
        apt = get_apt(apt_url, apt_id)
        # [{'url': p}]
        pic_url = apt['Bilde'][0]['url']
        addr = apt['Adresse']
        self.assertRegex(pic_url, '.*/320w/.*\.jpg$')
        self.assertEqual(addr, 'Hockeyveien 2, 1069 Oslo')
