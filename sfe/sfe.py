#import urllib.parse import urlparse
import logging
import urllib
from functools import wraps
import requests
#from bs4 import BeautifulSoup
from lxml import html
import airtable
from sfe.config import MAPPING, QUERY, DB

log = logging.getLogger(__name__)
LOCALONLY = False
# faking headers
HEADERS = {'user-agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; '+
                         'rv:57.0) Gecko/20100101 Firefox/57.0'}

def get_aptlink(url):
    """ Returns url and ID """
    next_url = url
    page = 1
    links = []
    log.debug("Getting apartment links")
    while next_url:
        log.debug("Requesting page %s", (str(page)))
        # don't hit finn.no every time...
        # TODO: should probably be some mock thingy
        if not LOCALONLY:
            log.debug("Requesting %s", next_url)
            res = requests.get(next_url, headers=HEADERS)
            res.raise_for_status()
            base_url = urllib.parse.urlparse(res.url)
            base_url = base_url.scheme+'://'+base_url.hostname
            with open('./tests/data/finn_search_{}.html'.format(str(page)), 'wb') as fd:
                for byteseq in res.iter_content(chunk_size=1024):
                    fd.write(byteseq)
            res_t = res.text
        else:
            file_ = './tests/data/finn_search_{}.html'.format(str(page))
            log.debug("Getting data from %s", file_)
            with open(file_) as fd:
                base_url = "https://finn.no"
                res_t = fd.read()

        #wo = BeautifulSoup(res_t, 'lxml')
        wo = html.fromstring(res_t)
        # how much logic should be put into this? using a[not...] to exclude
        # advertisement
        xpath_links = "//div[contains(@class, 'result-item')]/" \
                "a[not(@data-promoted-view-webstat-counter-id)]/@href"
        for href in wo.xpath(xpath_links):
            apt_id = get_urlparam(href, 'finnkode')
            links.append({'Url': base_url+href, 'apt_id': apt_id})
        xpath_nexturl = "string(//span/a[@rel='next']/@href)"
        next_url = str(wo.xpath(xpath_nexturl))
        if next_url:
            next_url = base_url + next_url
            page = int(get_urlparam(next_url, 'page'))
    log.debug("Got %s links", len(links))
    return links

def get_apt(url, apt_id):
    # don't hit finn.no every time...
    # TODO: should probably be some mock thingy
    import hashlib
    md5name = hashlib.md5(url.encode('utf-8')).hexdigest()
    file_ = './tests/data/finn_apt_{}.html'.format(md5name)
    log.debug("Using url %s", url)
    if not LOCALONLY:
        res = requests.get(url, headers=HEADERS)
        res.raise_for_status()
        with open(file_, 'wb') as fd:
            log.debug("Writing to file %s", file_)
            for byteseq in res.iter_content(chunk_size=1024):
                fd.write(byteseq)
        res_t = res.text
    else:
        log.debug("Getting data from %s", file_)
        with open(file_) as fd:
            res_t = fd.read()
    wo = html.fromstring(res_t)
    apt_obj = {translate_field('Url'): url, translate_field('Id'): apt_id}
    try:
        for field, properties in MAPPING.get('apartments').items():
            translated = translate_field(field)
            if properties.get("Selector"):
                apt_obj[translated] = wo.xpath(properties["Selector"])
                if properties.get("Function"):
                    log.debug("Running function %s on %s='%s'", properties["Function"],translated,apt_obj[translated])
                    apt_obj[translated] = properties["Function"](apt_obj[translated])
            log.debug("%-16s =  '%s' %s", translated, apt_obj.get(translated), type(apt_obj.get(translated)))
    except Exception as E:
        log.exception(E)
        raise
    return apt_obj



def get_urlparam(url, param):
    query = urllib.parse.urlparse(url).query
    return urllib.parse.parse_qs(query).get(param, [None])[0]

def handle_at(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        try:
            log.debug("Calling at function %s", func.__name__)
            return func(*args, **kwargs)
        except requests.exceptions.HTTPError as err:
            import json
            try:
                error_object = err.response.json().get('error', {})
                msg = (error_object.get('type', 'UNKNOWN TYPE') + ": " +
                       error_object.get('message', err.response.text))
                print(err.response.json())
                raise ValueError(msg)
            except json.decoder.JSONDecodeError:
                raise Exception(err)
    return wrapper

@handle_at
def create_new_row(at, id_row, **kwargs):
    result = None
    # This validation (look for match) happens twice - once with getting
    # all ids, another here...
    # TODO: consider this...
    match = at.match(id_row, kwargs[id_row])
    if not match:
        log.debug("Creating row with %s", kwargs)
        result = at.insert(kwargs)
    return result

@handle_at
def update_row(at, **kwargs):
    id_value = kwargs[translate_field('Id')]
    log.debug("Updating row with %s = %s", translate_field('Id'), id_value)
    return at.update_by_field(translate_field('Id'), id_value, kwargs)

@handle_at
def iterate_empty(at):
    # where address is empty
    formula_filter = "{{{}}} = ''".format(translate_field('Address'))
    fields = [translate_field('Id'), translate_field('Url')]
    for i in at.get_iter(filterByFormula=formula_filter, 
                         pageSize=3, fields=fields):
        yield i

@handle_at
def get_existing_ids(at):
    id_ = translate_field('Id')
    ids = at.get_all(fields=id_)
    return {k['id']: k['fields'].get(id_) for k in ids}

def translate_field(key):
    """ Returns translation from config.MAPPING if such exists """
    translation = MAPPING.get('apartments',
                              {}).get(key, {}).get('Translation')
    if not translation:
        translation = key
    log.debug("%s translation = %s", key, translation)
    return translation

def main():
    log.info('Application started')

    AIRTABLE_CONF = DB['api']
    at = airtable.Airtable(
        AIRTABLE_CONF['base'],
        'leilighet',
        AIRTABLE_CONF['api_key']
    )
    log.debug("Airtable connected, authenticated: %s, table: %s",
              at.is_authenticated, at.table_name)

    url_field = translate_field('Url')
    id_field = translate_field('Id')

    ids = get_existing_ids(at)
    assert isinstance(ids, dict), 'ids is not dict instance'
    log.debug("Id count %s", len(ids))
    firstpage_url = QUERY['baseurl'] + QUERY['query']
    
    create_count = 0

    for aptlink in get_aptlink(firstpage_url):
        aptlink[url_field] = aptlink.pop('Url')
        aptlink[id_field] = aptlink.pop('apt_id')
        aptlink[id_field] = int(aptlink[id_field])
        if aptlink[id_field] not in ids.values():
            log.debug("Creating row %s", aptlink)
            result = create_new_row(at, id_field, **aptlink)
            if result:
                create_count += 1
    log.info('Created %s rows', create_count)

    update_count = 0

    for empty in iterate_empty(at):
        log.debug("iterate length %s", len(empty))
        for apt in empty:
            if apt['fields'].get(url_field) and apt['fields'].get(id_field):
                result = get_apt(apt['fields'][url_field], apt['fields'][id_field])
                if update_row(at, **result):
                    update_count += 1
    log.info("Updated %s rows", update_count)

    log.info('Application finished')

if __name__ == '__main__':
    main()
