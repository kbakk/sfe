# -*- coding: utf-8 -*-
""" Configuration file for sfe pythin package """

import os

def _getenv(variable, default=None):
    envvar = os.getenv(variable)
    if envvar != None:
        return envvar
    elif default:
        return default
    else:
        raise ValueError("Environment variable {} not set".format(variable))

LOGGING = dict(
    version=1,
    formatters={
        'f': {'format':
              "%(relativeCreated)08.2f \033[95m%(levelname)-7s\033[0m\033[92m"\
              "%(name)s:\033[0m %(message)s"}},
    handlers={'h': {'class': 'logging.StreamHandler',
                    'formatter': 'f',
                    'level': 'DEBUG'}},
    root={'handlers': ['h'],
          'level': _getenv('SFE_LOGLEVEL','DEBUG')},
    disable_existing_loggers=False)

QUERY = {
    'baseurl': 'https://www.finn.no',
    'query': '/realestate/homes/search.html'+
             '?bbox=10.566903725266455%2C59.88672347158576%2C'+
             '10.99273357540369%2C59.9800611685988'+
             '&stored-id=13332846&q=balkong+OR+terrasse&no_of_bedrooms_from=2'+
             '&sort=1&area_from=45&price_collective_to=3300000'
}

DB = {
    'api': {
        'endpoint_url': 'https://api.airtable.com',
        'api_key': _getenv('AIRTABLE_API_KEY'),
        'base': _getenv('AIRTABLE_BASE')}}

def price_to_int(price):
    if not price:
        return 0
    price_nbsp_removed = price.replace(u'\xa0', u' ')
    return int(''.join( c for c in price_nbsp_removed if c not in ' ,-' ))
def rooms_to_int(rooms):
    try:
        return int(str(rooms).strip())
    except ValueError:
        return None
def picture_to_attachment(picture):
    if len(picture) < 1:
        return None
    p = picture.split(",")[-1].split()[0]
    return [{'url': p}]

MAPPING = {
    'apartments': {
        'Address': {'Translation': 'Adresse', 
                    'Selector': "string((//div/p[contains(@class,'maplink')]/preceding-sibling::h2/text())[last()])"},
        'Id':{'Translation': 'FINN-kode', 'Selector': None},
        'Url': {'Translation': None, 'Selector': None},
        'City': {'Translation': 'By', 
                 'Selector': "string(/html/body/div[2]/div/div[1]/div/div/div[1]/a[3])"}, #TODO: improve
        'Location': {'Translation': 'Sted', 
                     'Selector': "string(/html/body/div[2]/div/div[1]/div/div/div[1]/a[4])"}, #TODO: improve
        #'Price Estimate': 'Prisantydning',
        'Price Total': {'Translation': 'Prisantydning', 
                        'Selector': "string(//dt[contains(text(),'Totalpris')]/following-sibling::dd)", 
                        'Function': price_to_int},
        'Rent': {'Translation': 'Fellesutgifter', 
                 'Selector': "string(//dt[contains(text(),'Felleskost/mnd.')]/following-sibling::dd)", 
                 'Function': price_to_int},
        #'Floor': 'Etasje', - string(//dt[contains(text(),'Etasje')]/following-sibling::dd
        # Brutto areal - uncertain of EN translation
        'BTA_': {'Translation': 'BTA', 'Selector': "string(//dt[contains(text(),'Bruttoareal')]/following-sibling::dd)", 
                 'Function': lambda x: None if len(x) < 1 else int(str(x).split()[0])},
        #'Primary Room Area':'Primærrom',
        #'Construction Year': 'Byggeår',
        #'Energy Classification': 'Energimerke', - 
        'Bedrooms': {'Translation': 'Soverom', 'Selector': "string(//dt[contains(text(),'Soverom')]/following-sibling::dd)", 'Function': rooms_to_int},
        # wo.xpath("string(//img[@aria-label='Galleribilde']/@srcset[last()])").split(",")[-1].split()[0]
        'Picture': {'Translation': 'Bilde', 'Selector': "string(//img[@aria-label='Galleribilde']/@srcset[last()])", 
                    'Function': picture_to_attachment},
        #'Fellesgjeld_': 'Fellesgjeld',
        'Status': {'Translation': 'Status', 'Selector': None},
        'Criterias': {'Translation': 'Kriterier', 'Selector': None},
        'Next Viewing': {'Translation': 'Første visning', 'Selector': "string(//p/time)", 'Function':str.strip}}}

# h2
# div
# p class="maplink"
# //div[contains(@class,'bd') and ./p[contains(@class,'maplink')]/h2