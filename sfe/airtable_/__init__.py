""" Wrapper around the airtable module, handling connections etc. """
import airtable
import sfe.config as config

AIRTABLE_CONF = config.DB['api']
MAPPING = config.MAPPING


at = airtable.Airtable( # pylint: disable=invalid-name
    AIRTABLE_CONF['base'],
    'leilighet',
    AIRTABLE_CONF['api_key']
)

def return_at():
    print("Hello from {}".format(__name__))
    return at
