import logging
import logging.config

from .config import LOGGING
from .sfe import get_apt, get_aptlink

logging.getLogger(__name__).addHandler(logging.NullHandler())
logging.config.dictConfig(LOGGING)

log = logging
log.debug('Module %s initiated', __name__)
