sfe
===

A python package to **Search Finn Eiendom** for housing. 

Functionality
-------------

- Uses [Airtable](https://airtable.com/) as database and connects with (Airtable Python Wrapper)[https://github.com/gtalarico/airtable-python-wrapper]
- Collects apartments ads for a given search, and adds them to the database
- When detecting an ad change (e.g. price change), it updates the fields

Development
----

### References
* http://airtable-python-wrapper.readthedocs.io/en/latest/

### Todo

- [ ] don't throw a fit when failing to parse/update row

Airtable/database

- [x] create row for new results
- [x] updating empty
- [x] add image attachment
- [ ] update existing / update existing only with changes / update+handle missing fields
- [ ] function for connecting to and verifying DB
- [ ] add "new" status for every created item (requires translation mapping for statuses)

Querying Finn.no

- [x] function for searching Finn.no
- [x] getting search result - xpath links, next page
- [x] getting apartment - xpath properties
- [ ] parse remaining columns

### Ideas

- Use BeautifulSoup to remove all unneeded formatting, leaving only paragraphs, heading, table, simple formatting. Store it in a separate table, and use something like git diff to show difference between runs.


Requirements
------------

- `pip install -r requirements.txt`
    - Package is using requests, airtable (https://github.com/bayesimpact/airtable-python), lxml
-  Environment variables:
    - `AIRTABLE_API_KEY` (get it on the [airtable.com account page](https://airtable.com/account)) 
    - `AIRTABLE_BASE` - ID of base (go to the base, select questionmark, API documentation)